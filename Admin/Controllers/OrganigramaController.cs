﻿using Admin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers
{
    public class OrganigramaController : Controller
    {
        private Modelo db = new Modelo();
        // GET: Organigrama
        public ActionResult Index()
        {
            var imagenes = db.ORGANIGRAMA.ToList();
            return View(imagenes);
        }


        public ActionResult Details(int? id)
        {
            var imagen = db.ORGANIGRAMA.Find(id);
            return View(imagen);
        }


        public ActionResult Editar(int? id)
        {
            var grupos = db.ORGANIGRAMA.Select(x => x.GRUPO).Distinct().ToList();
            //var groups = new SelectList(grupos, gruo, "GRUPO");
            //ViewData["grupos"] = groups;

            var query = db.ORGANIGRAMA.Find(id);
            OrganigramaView imagen = new OrganigramaView();
            imagen.ID = query.ID;
            imagen.APARTADO = query.APARTADO;
            imagen.DESCRIPCION = query.DESCRIPCION;
            imagen.GRUPO = query.GRUPO;
            imagen.IMAGEN = query.IMAGEN;
            return View(imagen);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(OrganigramaView ImagenesView)
        {
            if (ModelState.IsValid)
            {
                var pic = string.Empty;
                var folder = "/images/organigrama";

                if (ImagenesView.ImagenFile != null)
                {
                    pic = FilesHelper.UploadPhoto(ImagenesView.ImagenFile, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }

                var imagen = ToImagenes(ImagenesView);
                imagen.IMAGEN = pic;
                db.Entry(imagen).State = EntityState.Modified;
                db.SaveChanges();

                return Redirect("/Organigrama/Details/" + ImagenesView.ID);

            }
            return View();
        }


        private ORGANIGRAMA ToImagenes(OrganigramaView view)
        {
            return new ORGANIGRAMA
            {
                ID = view.ID,
                APARTADO = view.APARTADO,
                IMAGEN = view.IMAGEN,
                ORDEN = view.ORDEN,
                DESCRIPCION = view.DESCRIPCION,
                GRUPO = view.GRUPO
            };
        }
    }
}