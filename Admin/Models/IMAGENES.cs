namespace Admin.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PORTAL.IMAGENES")]
    public partial class IMAGENES
    {
        public int ID { get; set; }

        [StringLength(100)]
        public string APARTADO { get; set; }

        public string IMAGEN { get; set; }

        public DateTime? FECHA { get { return DateTime.Now; } }

        public int? ORDEN { get; set; }

        [StringLength(50)]
        public string DESCRIPCION { get; set; }
    }
}
