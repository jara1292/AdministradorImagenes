﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Admin.Models
{
    [NotMapped]
    public class OrganigramaView: ORGANIGRAMA
    {
        public HttpPostedFileBase ImagenFile { get; set; }
    }
}