﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Admin.Models;
using System.Data.SqlClient;

namespace Admin.Controllers
{
    public class SliderController : Controller
    {
        private Modelo db = new Modelo();
        // GET: Slider
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FonixEresTu()
        {
            var imagenes = db.IMAGENES.Where(x => x.APARTADO == "FONIX_ERES_TU").OrderBy(x => x.ORDEN);
            return View(imagenes.ToList());
        }


        public ActionResult Agregar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Agregar(ImagenesView ImagenesView)
        {
            if (ModelState.IsValid)
            {
                var url = "";
                var pic = string.Empty;
                var folder = "";
                if (ImagenesView.APARTADO == "FONIX_ERES_TU")
                {
                    folder = "/imagenes/eres_tu";
                    url = "FonixEresTu";
                }
                else if (ImagenesView.APARTADO == "EVENTOS")
                {
                    folder = "/imagenes/eventos";
                    url = "Eventos";
                }
                else if (ImagenesView.APARTADO == "BIENVENIDO")
                {
                    folder = "/imagenes/bienvenido";
                    url = "Bienvenidos";
                }
                else { folder = "/imagenes/clasificados"; url = "Clasificados"; }
                using (var transaccion = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (ImagenesView.ImagenFile != null)
                        {
                            pic = FilesHelper.UploadPhoto(ImagenesView.ImagenFile, folder);
                            pic = string.Format("{0}/{1}", folder, pic);
                        }

                        var imagen = ToImagenes(ImagenesView);
                        imagen.IMAGEN = pic;
                        db.IMAGENES.Add(imagen);
                        transaccion.Commit();
                        db.SaveChanges();


                        return RedirectToAction(url);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Error: " + ex.Message + "//" + ex.InnerException;
                        return View(ImagenesView);

                    }

                }


            }

            return View(ImagenesView);

        }

        public ActionResult Comunicados()
        {
            var imagenes = db.IMAGENES.Where(x => x.APARTADO == "COMUNICADOS").OrderBy(x => x.ORDEN);
            return View(imagenes.ToList());
        }

        public ActionResult Clasificados()
        {
            var imagenes = db.IMAGENES.Where(x => x.APARTADO == "CLASIFICADOS").OrderBy(x => x.ORDEN);
            return View(imagenes.ToList());
        }

        public ActionResult Eventos()
        {
            var imagenes = db.IMAGENES.Where(x => x.APARTADO == "EVENTOS").OrderBy(x => x.ORDEN);
            return View(imagenes.ToList());
        }


        public ActionResult Bienvenidos()
        {
            var imagenes = db.IMAGENES.Where(x => x.APARTADO == "BIENVENIDO").OrderBy(x => x.ORDEN);
            return View(imagenes.ToList());
        }


        private IMAGENES ToImagenes(ImagenesView view)
        {
            return new IMAGENES
            {
                ID = view.ID,
                APARTADO = view.APARTADO,
                IMAGEN = view.IMAGEN,
                ORDEN = view.ORDEN,
                DESCRIPCION = view.DESCRIPCION
            };
        }



        public ActionResult EliminarImagen(int? id)
        {
            var imagen = db.IMAGENES.Find(id);
            return View(imagen);
        }


        public ActionResult ColoresCumple()
        {
            var imagenes = db.IMAGENES.Where(x => x.APARTADO == "CUMPLEANIOS").OrderBy(x => x.ORDEN);
            return View(imagenes.ToList());
        }


        public void ActualizarColores(string colorf,string colorn, string colord)
        {

            var query = db.Database.ExecuteSqlCommand("UPDATE PORTAL.IMAGENES set DESCRIPCION=@color1 WHERE ID=124", new SqlParameter("@color1", colorf));
            var query2 = db.Database.ExecuteSqlCommand("UPDATE PORTAL.IMAGENES set DESCRIPCION=@color2 WHERE ID=125", new SqlParameter("@color2", colorn));
            var query3 = db.Database.ExecuteSqlCommand("UPDATE PORTAL.IMAGENES set DESCRIPCION=@color3 WHERE ID=126", new SqlParameter("@color3", colord));

            //return OK;

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EliminarImagen(IMAGENES imagen)
        {
            var query = db.IMAGENES.Find(imagen.ID);

            db.IMAGENES.Remove(query);
            db.SaveChanges();

            if (query.APARTADO == "FONIX_ERES_TU")
            {
                return RedirectToAction("FonixEresTu");
            }
            else if (query.APARTADO == "EVENTOS")
            {
                return RedirectToAction("Eventos");
            }
            else if (query.APARTADO == "BIENVENIDO")
            {
                return RedirectToAction("Bienvenidos");
            }
            else { return RedirectToAction("Clasificados"); }

        }
    }
}