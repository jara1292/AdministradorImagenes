﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Admin.Models
{
    [Table("PORTAL.IMAGENES_ORGANIGRAMA")]
    public class ORGANIGRAMA
    {
        public int ID { get; set; }

        [StringLength(100)]
        public string APARTADO { get; set; }

        public string IMAGEN { get; set; }

        public DateTime? FECHA { get { return DateTime.Now; } }

        public int? ORDEN { get; set; }

        [StringLength(50)]
        public string DESCRIPCION { get; set; }


        [StringLength(50)]
        public string GRUPO { get; set; }
    }
}