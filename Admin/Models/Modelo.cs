namespace Admin.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Modelo : DbContext
    {
        public Modelo()
            : base("name=Modelo")
        {
        }

        public virtual DbSet<IMAGENES> IMAGENES { get; set; }
        public virtual DbSet<ORGANIGRAMA> ORGANIGRAMA { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IMAGENES>()
                .Property(e => e.APARTADO)
                .IsUnicode(false);

            modelBuilder.Entity<IMAGENES>()
                .Property(e => e.IMAGEN)
                .IsUnicode(false);

            modelBuilder.Entity<IMAGENES>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);
        }
    }
}
