﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Admin.Models;
using System.Data.Entity;

namespace Admin.Controllers
{
    public class EnlacesController : Controller
    {
        private Modelo db = new Modelo();
        // GET: Enlaces
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult ActualizarImagen()
        {
            string imagen = Request.QueryString["imagen"];
            var query = db.IMAGENES.Where(x => x.DESCRIPCION == imagen);
            return View(query.FirstOrDefault());
        }


        public ActionResult Editar(int id)
        {
            var query = db.IMAGENES.Find(id);
            ImagenesView imagen = new ImagenesView();
            imagen.ID = query.ID;
            imagen.APARTADO = query.APARTADO;
            imagen.DESCRIPCION = query.DESCRIPCION;
            imagen.IMAGEN = query.IMAGEN;
            return View(imagen);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(ImagenesView ImagenesView)
        {
            if (ModelState.IsValid)
            {
                var pic = string.Empty;
                var folder = "/imagenes/enlacelat";

                if (ImagenesView.ImagenFile != null)
                {
                    pic = FilesHelper.UploadPhoto(ImagenesView.ImagenFile, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }

                var imagen = ToImagenes(ImagenesView);
                imagen.IMAGEN = pic;
                db.Entry(imagen).State = EntityState.Modified;
                db.SaveChanges();

                return Redirect("/Enlaces/ActualizarImagen?imagen=" + ImagenesView.DESCRIPCION);

            }
            return View();
        }




        private IMAGENES ToImagenes(ImagenesView view)
        {
            return new IMAGENES
            {
                ID = view.ID,
                APARTADO = view.APARTADO,
                IMAGEN = view.ImagenFile.FileName,
                ORDEN = view.ORDEN,
                DESCRIPCION = view.DESCRIPCION
            };
        }
    }
}