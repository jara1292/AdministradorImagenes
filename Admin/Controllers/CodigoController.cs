﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Admin.Models;
using System.Data.Entity;

namespace Admin.Controllers
{
    public class CodigoController : Controller
    {
        private Modelo db = new Modelo();
        // GET: Codigo
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Actualizar()
        {
            var imagen = Request.QueryString["tipo"];
            var query = db.IMAGENES.Where(x => x.DESCRIPCION == imagen);
            return View(query.ToList());
        }


        public ActionResult Editar(int id)
        {
            var query = db.IMAGENES.Find(id);
            ImagenesView imagen = new ImagenesView();
            imagen.ID = query.ID;
            imagen.APARTADO = query.APARTADO;
            imagen.DESCRIPCION = query.DESCRIPCION;
            imagen.IMAGEN = query.IMAGEN;
            return View(imagen);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(ImagenesView ImagenesView)
        {
            if (ModelState.IsValid)
            {
                var pic = string.Empty;
                var folder = "/imagenes/codigo_etica";

                if (ImagenesView.ImagenFile != null)
                {
                    pic = FilesHelper.UploadPhoto(ImagenesView.ImagenFile, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }

                var imagen = ToImagenes(ImagenesView);
                imagen.IMAGEN = pic;
                db.Entry(imagen).State = EntityState.Modified;
                db.SaveChanges();

                return Redirect("/Admin/Codigo/Actualizar?tipo=" + ImagenesView.DESCRIPCION);

            }
            return View();
        }


        private IMAGENES ToImagenes(ImagenesView view)
        {
            return new IMAGENES
            {
                ID = view.ID,
                APARTADO = view.APARTADO,
                IMAGEN = view.IMAGEN,
                ORDEN = view.ORDEN,
                DESCRIPCION = view.DESCRIPCION
            };
        }
    }
}